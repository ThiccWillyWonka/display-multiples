/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example;

import java.util.Scanner;

public class DisplayMultiples {

    public static void main(String args[]) {
       Scanner sc = new Scanner(System.in);
       System.out.print("Please enter a number : ");
       int num = sc.nextInt();
       System.out.println("The multiples are : "); 
        for (int i = 1; i<=12;i++){
            System.out.println( i+" x "+num+" = "+(num*i));
        }   
    }
}
